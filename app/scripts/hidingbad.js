var HidingBad = HidingBad || (function() {
  var _public = {};

  var terms = [
    '#breakingbad',
    '#BREAKINGBAD',
    '#BreakingBad',
    'breakingbad',
    'BREAKINGBAD',
    'BreakingBad',
    'BREAKING BAD',
    'breaking bad',
    'Breaking Bad',
    "'BREAKING BAD'",
    "'Breaking Bad'",
    "'breaking bad'"
  ];

  var searchTerms = [];
  var searchString = '';
  var alerted = false;
  var $hiddenItems;

  function init() {
    searchTerms = [];
    $.each(terms, function(i) {
      // console.log(i);
      searchTerms[i] = '*:contains("' + terms[i] + '")';
    });

    searchString = searchTerms.join();

    $hiddenItems = $(searchString);

    if ($hiddenItems.length > 0) {
      $hiddenItems.each(function(){
        // if ($(this).children().length < 1) {
          $(this).closest('p, li').css({
            opacity: '0.02'
          });
          // $(this).closest('p, li').html('<div>This content has been hidden.</div>');
        // }
      });
      if (alerted === false) {
        alerted = true;
        alert("Instances of Breaking Bad have been hidden on this page. You may want to leave to avoid any other spoilers!");
      }
    }
  }

  _public.init = init;

  return _public;
})();

var timeout = null;
document.addEventListener('DOMSubtreeModified', function() {
  if (timeout) {
    clearTimeout(timeout);
  }
  timeout = setTimeout(HidingBad.init, 500);
}, false);

HidingBad.init();
