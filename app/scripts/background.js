'use strict';

// chrome.runtime.onInstalled.addListener(function (details) {});
// chrome.tabs.onCreated.addListener(function(tab) {});

var buttonStatus = false;

chrome.browserAction.onClicked.addListener(function(tab) {
  buttonStatus = !buttonStatus;
  if (buttonStatus) {
    chrome.browserAction.setIcon({path: '../images/icon-38-on.png', tabId:tab.id});
    chrome.tabs.executeScript(tab.id, {code:'init'});
  } else {
    chrome.browserAction.setIcon({path: '../images/icon-38-off.png', tabId:tab.id});
    chrome.tabs.executeScript(tab.id, {code:'init'});
  }
});
